import React from 'react';

import './App.css';
import Item from './components/Item';

function App () {
  const list = ['Item 1', 'Item 2', 'Item 3'];

  return (
    <div className="conteudos">
        <div>Item 1</div>
        <div>Item 2</div>
        <div>Item 3</div>
        {list.map((text, index) => (
          <Item key={text} text={text} index={index} />
        ))}
    </div>       
  );
}

export default App